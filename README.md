# CS371p: Object-Oriented Programming Allocator Repo

* Name: Brandt Swanson

* EID: bas4932

* GitLab ID: brandtswan

* HackerRank ID: brandtswan

* Git SHA: 8211b05cf8b1294e469e072a43fd2f9ebe1e579d

* GitLab Pipelines: https://gitlab.com/brandtswan/cs371p-allocator/-/pipelines

* Estimated completion time: 30

* Actual completion time: 40

* Comments: I didn't pay enough attention to the due date when the project was assigned. I'll make sure not to make the same mistake for the next project! I also had a lot of stupid mistakes that held me back with the HackerRank, and that ended up taking a lot of time to fix.
