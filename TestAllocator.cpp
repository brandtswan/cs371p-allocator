// -----------------
// TestAllocator.c++
// -----------------

// https://github.com/google/googletest
// https://github.com/google/googletest/blob/master/googletest/docs/primer.md
// https://github.com/google/googletest/blob/master/googletest/docs/advanced.md

// --------
// includes
// --------

#include <algorithm> // count
#include <cstddef>   // ptrdiff_t
#include <memory>    // allocator

#include "gtest/gtest.h"

#include "Allocator.hpp"

TEST(AllocatorFixture, test0) {
    using allocator_type = std::allocator<int>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    allocator_type   x;
    const size_type  s = 10;
    const value_type v = 2;
    const pointer    b = x.allocate(s);
    ASSERT_NE(b, nullptr);
    if (b != nullptr) {
        pointer e = b + s;
        pointer p = b;
        try {
            while (p != e) {
                x.construct(p, v);
                ++p;}}
        catch (...) {
            while (b != p) {
                --p;
                x.destroy(p);}
            x.deallocate(b, s);
            throw;}
        ASSERT_EQ(std::count(b, e, v), ptrdiff_t(s));
        while (b != e) {
            --e;
            x.destroy(e);}
        x.deallocate(b, s);}}

TEST(AllocatorFixture, test1) {
    using allocator_type = my_allocator<int, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    allocator_type   x;
    const size_type  s = 10;
    const value_type v = 2;
    const pointer    b = x.allocate(s);
    ASSERT_NE(b, nullptr);
    if (b != nullptr) {
        pointer e = b + s;
        pointer p = b;
        try {
            while (p != e) {
                x.construct(p, v);
                ++p;}}
        catch (...) {
            while (b != p) {
                --p;
                x.destroy(p);}
            x.deallocate(b, s);
            throw;}
        ASSERT_EQ(std::count(b, e, v), ptrdiff_t(s));
        while (b != e) {
            --e;
            x.destroy(e);}
        x.deallocate(b, s);}}

                                                               // uncomment
TEST(AllocatorFixture, test2) {
    using allocator_type = my_allocator<int, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    allocator_type x;                                            // read/write
    ASSERT_EQ(x[0], 992);}                                         // fix test

TEST(AllocatorFixture, test3) {
    using allocator_type = my_allocator<int, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    const allocator_type x;                                      // read-only
    ASSERT_EQ(x[0], 992);}                                         // fix test


TEST(AllocatorFixture, test4) {
    using allocator_type = my_allocator<int, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    allocator_type x;
    x.allocate(5);
    ASSERT_EQ(x[0], -20);
    ASSERT_EQ(x[996], 964);}                                         // fix test

TEST(AllocatorFixture, test5) {
    using allocator_type = my_allocator<int, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    allocator_type x;
    x.allocate(5);
    x.allocate(10);
    ASSERT_EQ(x[0], -20);
    ASSERT_EQ(x[28], -40);}                                         // fix test

TEST(AllocatorFixture, test6) {
    using allocator_type = my_allocator<int, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    allocator_type x;
    x.allocate(10);
    ASSERT_EQ(x[0], -40);}

TEST(AllocatorFixture, test7) {
    using allocator_type = my_allocator<int, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    allocator_type x;
    x.allocate(246);
    ASSERT_EQ(x[0], -992);
    ASSERT_EQ(x[996], -992);}

TEST(AllocatorFixture, test8) {
    using allocator_type = my_allocator<int, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    allocator_type x;
    pointer p = x.allocate(5);
    x.deallocate(p, sizeof(int));
    ASSERT_EQ(x[0], 992);
    ASSERT_EQ(x[996], 992);}                                         // fix test

TEST(AllocatorFixture, test9) {
    using allocator_type = my_allocator<double, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    allocator_type x;
    pointer p = x.allocate(5);
    pointer b = x.allocate(3);
    x.allocate(3);
    x.deallocate(p, sizeof (int));
    x.deallocate(b, sizeof (int));
    x.print();}                                         // fix test

TEST(AllocatorFixture, test10) {
using allocator_type = my_allocator<double, 1000>;
using value_type     = typename allocator_type::value_type;
using size_type      = typename allocator_type::size_type;
using pointer        = typename allocator_type::pointer;

    allocator_type x;
    x.allocate(5);
    pointer b = x.allocate(3);
    x.allocate(3);
    x.deallocate(b, sizeof (int));
    x.print();}                                         // fix test

TEST(AllocatorFixture, test11) {
    using allocator_type = my_allocator<double, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    allocator_type x;
    x.allocate(5);
    pointer b = x.allocate(3);
    pointer c = x.allocate(3);
    x.deallocate(b, sizeof (int));
    x.deallocate(c, sizeof (int));
    x.print();
    ASSERT_EQ(x[996], 944);}                                         // fix test

TEST(AllocatorFixture, test12) {
    using allocator_type = my_allocator<double, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    allocator_type x;
    pointer a = x.allocate(5);
    pointer b = x.allocate(3);
    pointer c = x.allocate(3);
    x.deallocate(b, sizeof (int));
    x.deallocate(c, sizeof (int));
    x.deallocate(a, sizeof (int));
    x.print();
    ASSERT_EQ(x[996], 992);}                                         // fix test

TEST(AllocatorFixture, test13) {
    using allocator_type = my_allocator<double, 1000>;
    allocator_type a;
    allocator_type b;
    ASSERT_TRUE(a != b);}

TEST(AllocatorFixture, test14) {
    my_allocator<double, 1000> a;
    my_allocator<double, 1000> b;
    ASSERT_FALSE(a == b);}                                         // fix test


TEST(AllocatorFixture, test15) {
    my_allocator<double, 1000> a;
    a.allocate(1);
    ASSERT_EQ(a[0],-8);
    ASSERT_EQ(a[16],976);}                                         // fix test

TEST(AllocatorFixture, test16) {
    using allocator_type = my_allocator<double, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    allocator_type x;
    pointer a = x.allocate(1);
    pointer b = x.allocate(2);
    pointer c = x.allocate(3);
    pointer d = x.allocate(4);
    pointer e = x.allocate(5);
    pointer f = x.allocate(6);
    pointer g = x.allocate(7);
    x.deallocate(a, sizeof (double));
    x.deallocate(b, sizeof (double));
    x.deallocate(c, sizeof (double));
    x.deallocate(d, sizeof (double));
    x.deallocate(f, sizeof (double));
    x.allocate(3);
    x.allocate(4);
    x.print();}                                         // fix test

TEST(AllocatorFixture, test17) {
    using allocator_type = my_allocator<double, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    allocator_type x;
    pointer a = x.allocate(1);
    pointer b = x.allocate(2);
    pointer c = x.allocate(3);
    pointer d = x.allocate(4);
    pointer e = x.allocate(5);
    pointer f = x.allocate(6);
    pointer g = x.allocate(7);
    x.deallocate(a, sizeof (double));
    x.deallocate(b, sizeof (double));
    x.deallocate(c, sizeof (double));
    x.deallocate(d, sizeof (double));
    x.deallocate(f, sizeof (double));
    x.allocate(3);
    x.allocate(4);
    x.print();}                                         // fix test
//10
//11
//12
//13
//14
//13
//-2
//-3
//-4
//1
//1
//1
//1
//1
//1
//1
//-1
//-2
//-3
TEST(AllocatorFixture, test18) {
    using allocator_type = my_allocator<double, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    allocator_type x;
    pointer a = x.allocate(10);
    pointer b = x.allocate(11);
    pointer c = x.allocate(12);
    pointer d = x.allocate(13);
    pointer e = x.allocate(14);
    pointer f = x.allocate(13);
    x.deallocate(b, sizeof (double));
    x.deallocate(d, sizeof (double));
    x.deallocate(f, sizeof (double));
    x.allocate(1);
    pointer g = x.allocate(1);
    x.allocate(1);
    pointer h = x.allocate(1);
    x.allocate(1);
    x.allocate(1);
    x.allocate(1);
    x.deallocate(a, sizeof (double));
    x.deallocate(g, sizeof (double));
    x.deallocate(h, sizeof (double));
    x.print();}                                         // fix test

TEST(AllocatorFixture, test19) {
using allocator_type = my_allocator<double, 1000>;
using value_type     = typename allocator_type::value_type;
using size_type      = typename allocator_type::size_type;
using pointer        = typename allocator_type::pointer;

    allocator_type x;
    pointer a = x.allocate(10);
    pointer b = x.allocate(11);
    pointer c = x.allocate(12);
    pointer d = x.allocate(13);
    pointer e = x.allocate(14);
    pointer f = x.allocate(13);
    x.deallocate(b, sizeof (double));
    x.deallocate(d, sizeof (double));
    x.deallocate(f, sizeof (double));
    x.allocate(1);
    pointer g = x.allocate(1);
    x.allocate(1);
    pointer h = x.allocate(1);
    x.allocate(1);
    x.allocate(1);
    x.deallocate(a, sizeof (double));
    x.deallocate(g, sizeof (double));
    x.deallocate(e, sizeof (double));
    x.print();}                                         // fix test