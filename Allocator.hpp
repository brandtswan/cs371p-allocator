// -----------
// Allocator.h
// -----------

#ifndef Allocator_h
#define Allocator_h

// --------
// includes
// --------

#include <cassert>   // assert
#include <cstddef>   // ptrdiff_t, size_t
#include <new>       // bad_alloc, new
#include <stdexcept> // invalid_argument

// ---------
// Allocator
// ---------

template <typename T, std::size_t N>
class my_allocator {
    // -----------
    // operator ==
    // -----------

    friend bool operator == (const my_allocator&, const my_allocator&) {
        return false;
    }                                                   // this is correct

    // -----------
    // operator !=
    // -----------

    friend bool operator != (const my_allocator& lhs, const my_allocator& rhs) {
        return !(lhs == rhs);
    }

public:
    // --------
    // typedefs
    // --------

    using      value_type = T;

    using       size_type = std::size_t;
    using difference_type = std::ptrdiff_t;

    using       pointer   =       value_type*;
    using const_pointer   = const value_type*;

    using       reference =       value_type&;
    using const_reference = const value_type&;

public:
    // ---------------
    // iterator
    // over the blocks
    // ---------------

    class iterator {
        // -----------
        // operator ==
        // -----------

        friend bool operator == (const iterator& lhs, const iterator& rhs) {
            return lhs._p == rhs._p;
        }                                           // replace!

        // -----------
        // operator !=
        // -----------

        friend bool operator != (const iterator& lhs, const iterator& rhs) {
            return !(lhs == rhs);
        }

    private:
        // ----
        // data
        // ----

        int* _p;
        int sentSize = sizeof(int);

    public:
        // -----------
        // constructor
        // -----------

        iterator (int* p) {
            _p = p;
        }

        // ----------
        // operator *
        // ----------

        int& operator * () const {
            return *_p;
        }           // replace!

        // ----------
        // operator +
        // ----------

        int* operator + (int bytes) const {
            return reinterpret_cast<int*>(reinterpret_cast<char*>(_p) + bytes);
        }

        // -----------
        // operator ++
        // -----------

        iterator& operator ++ () {
            int value = *_p;
            int blockSize = std::abs(value) + (2 * sentSize);
            _p = (int *)((char *)_p + blockSize);
            return *this;
        }

        // -----------
        // operator ++
        // -----------

        iterator operator ++ (int) {
            iterator x = *this;
            ++*this;
            return x;
        }

        // ----------
        // operator -
        // ----------

        int* operator - (int bytes) const {
            return reinterpret_cast<int*>(reinterpret_cast<char*>(_p) - bytes);
        }

        // -----------
        // operator --
        // -----------

        iterator& operator -- () {
            int value = *_p;
            int blockSize = std::abs(value) + (2 * sentSize);
            _p = (int *)((char *)_p - blockSize);
            return *this--;
        }

        // -----------
        // operator --
        // -----------

        iterator operator -- (int) {
            iterator x = *this;
            --*this;
            return x;
        }


        int* address() {
            return _p;
        }
    };

    // ---------------
    // const_iterator
    // over the blocks
    // ---------------

    class const_iterator {
        // -----------
        // operator ==
        // -----------

        friend bool operator == (const const_iterator& lhs, const const_iterator& rhs) {
            return lhs._p == rhs._p;
        }                                                       // replace!

        // -----------
        // operator !=
        // -----------

        friend bool operator != (const const_iterator& lhs, const const_iterator& rhs) {
            return !(lhs == rhs);
        }

    private:
        // ----
        // data
        // ----

        const int* _p;
        const int sentSize = sizeof(int);

    public:
        // -----------
        // constructor
        // -----------

        const_iterator (const int* p) {
            _p = p;
        }

        // ----------
        // operator *
        // ----------

        const int& operator * () const {
            return *_p;
        }                 // replace!

        // -----------
        // operator ++
        // -----------

        const_iterator& operator ++ () {
            int value = *_p;
            int blockSize = std::abs(value) + (2 * sentSize);
            _p = (int *)((char *)_p + blockSize);
            return *this;
        }

        // -----------
        // operator ++
        // -----------

        const_iterator operator ++ (int) {
            const_iterator x = *this;
            ++*this;
            return x;
        }

        // -----------
        // operator --
        // -----------

        const_iterator& operator -- () {
            int value = *_p;
            int blockSize = std::abs(value) + (2 * sentSize);
            _p = (int *)((char *)_p - blockSize);
            return *this;
        }

        // -----------
        // operator --
        // -----------

        const_iterator operator -- (int) {
            const_iterator x = *this;
            --*this;
            return x;
        }
    };

private:
    // ----
    // data
    // ----

    char a[N];
    const int sentSize = sizeof(int);

    // -----
    // valid
    // -----

    /**
     * O(1) in space
     * O(n) in time
     * Free blocks not adjacent
     * Gets to end of blocks correctly
     */
    bool valid () const {
        bool lastFree = false;
        for(const_iterator current = begin(); current != end(); current++) {
            if(*current < 0) {
                //Not free
                lastFree = false;
            } else {
                //Free
                if(lastFree)
                    return false;
                lastFree = true;
            }
        }
        return true;
    }

public:
    // -----------
    // constructor
    // -----------

    /**
     * O(1) in space
     * O(1) in time
     * throw a bad_alloc exception, if N is less than sizeof(T) + (2 * sizeof(int))
     */
    my_allocator () {
        if (N < sizeof(T) + 2 * sentSize)
            throw std::bad_alloc();
        (*this)[0] = N - 8;
        (*this)[N-4] = N - 8;
        assert(valid());
    }

    my_allocator             (const my_allocator&) = default;
    ~my_allocator            ()                    = default;
    my_allocator& operator = (const my_allocator&) = default;

    // --------
    // allocate
    // --------

    /**
     * O(1) in space
     * O(n) in time
     * after allocation there must be enough space left for a valid block
     * the smallest allowable block is sizeof(T) + (2 * sizeof(int))
     * choose the first block that fits
     * throw a bad_alloc exception, if n is invalid
     */
    pointer allocate (size_type size) {
        int newBlockSize = size * sizeof(T);
        if(newBlockSize >= (int)N || size <= 0)
            throw std::bad_alloc();
        for(iterator current = begin(); current != end(); current++) {
            int curBlockSize = *current;
            if(curBlockSize > (int)(newBlockSize + sizeof(T) + sentSize)) {
                int* firstSent = current.address();
                *firstSent = -newBlockSize;
                int* lastSent = current + (newBlockSize + sentSize);
                *lastSent = -newBlockSize;
                int* thirdSent = lastSent + 1;
                *thirdSent = curBlockSize - newBlockSize - sentSize * 2;
                int* fourthSent = thirdSent + ((curBlockSize - newBlockSize)/4 - 1);
                *fourthSent = curBlockSize - newBlockSize - sentSize * 2;
                assert(valid());
                return reinterpret_cast<T*>(firstSent + 1);
            } else if (curBlockSize >= newBlockSize) {
//                if (curBlockSize == (int)(newBlockSize + sizeof(T) + sentSize)) {
//                    newBlockSize = curBlockSize;
//                }
                int* firstSent = current.address();
                *firstSent = -curBlockSize;
                int* secondSent = current + (curBlockSize + sentSize);
                *secondSent = -curBlockSize;
                assert(valid());
                return reinterpret_cast<T*>(firstSent + 1);
            }
        }
        throw std::bad_alloc();
    }

    // ---------
    // construct
    // ---------

    /**
     * O(1) in space
     * O(1) in time
     */
    void construct (pointer p, const_reference v) {
        new (p) T(v);                               // this is correct and exempt
        assert(valid());
    }                           // from the prohibition of new

    // ----------
    // deallocate
    // ----------

    /**
     * O(1) in space
     * O(1) in time
     * after deallocation adjacent free blocks must be coalesced
     * throw an invalid_argument exception, if p is invalid
     * <your documentation>
     */
    void deallocate (pointer p, size_type) {
        int* start = (int*) ((char*) p - sizeof(int));
        int size = std::abs(*start);
        int* finish = (int*)((char*)start + sentSize + size);
        if(finish+1 < end().address() && *(finish+1)>0) {
            finish++;
            size += *finish + 2 * sentSize;
            finish += (*finish/4) + 1;
        }
        if(start > begin().address() && *(start-1)>0) {
            destroy(p);
            start--;
            int temp = *start;
            start -= (temp / 4) +1;
            size += temp + 2 * sentSize;
        }
        *start = size;
        *finish = size;

        assert(valid());
    }

    // -------
    // destroy
    // -------

    /**
     * O(1) in space
     * O(1) in time
     */
    void destroy (pointer p) {
        p->~T();               // this is correct
        assert(valid());
    }

    /**
      O(1) in space
     * O(1) in time
     */
    void print () {
        for(iterator current = begin(); current != end(); current++) {
            if(current != begin())
                std::cout << " ";
            std::cout << *current;
        }
    }

    // -----------
    // operator []
    // -----------

    /**
     * O(1) in space
     * O(1) in time
     */
    int& operator [] (int i) {
        return *reinterpret_cast<int*>(&a[i]);
    }

    /**
     * O(1) in space
     * O(1) in time
     */
    const int& operator [] (int i) const {
        return *reinterpret_cast<const int*>(&a[i]);
    }

    // -----
    // begin
    // -----

    /**
     * O(1) in space
     * O(1) in time
     */
    iterator begin () {
        return iterator(&(*this)[0]);
    }

    /**
     * O(1) in space
     * O(1) in time
     */
    const_iterator begin () const {
        return const_iterator(&(*this)[0]);
    }

    // ---
    // end
    // ---

    /**
     * O(1) in space
     * O(1) in time
     */
    iterator end () {
        return iterator(&(*this)[N]);
    }

    /**
     * O(1) in space
     * O(1) in time
     */
    const_iterator end () const {
        return const_iterator(&(*this)[N]);
    }
};

#endif // Allocator_h
