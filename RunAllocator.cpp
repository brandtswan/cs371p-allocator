// ----------------
// RunAllocator.c++
// ----------------

// --------
// includes
// --------


#include <cassert>  // assert
#include <iostream> // endl, istream, ostream
#include <sstream>  // istringstream
#include <string>   // getline, string
#include <vector>
#include <algorithm>    // std::sort
#include <tuple>    // make_tuple, tie, tuple
#include <utility>  // make_pair, pair

#include "Allocator.hpp"

using namespace std;
// ----
// main
// ----
double get_single_number (const string& s) {
    istringstream sin(s);
    double t;
    sin >> t;
    return t;
}

int main () {
    /*
    your code for the read eval print loop (REPL) goes here
    in this project, the unit tests will only be testing Allocator.hpp, not the REPL
    the acceptance tests will be testing the REPL
    the acceptance tests are hardwired to use my_allocator<double, 1000>
    */
    //Find number of test cases (t)
    int numCases;
    string s;
    cin >> numCases;
    getline(cin, s);
    getline(cin, s);
    for(int x = 1; x <= numCases; x++) {
        my_allocator<double, 1000> allocator;
        std::vector<double*> numbers;
        while(!cin.eof()) {
            getline(cin, s);
            if(s.empty()) {
                break;
            }
            double num = get_single_number(s);
            if(num > 0) {
                numbers.emplace_back(allocator.allocate(num));
                std::sort(numbers.begin(),numbers.end());
            } else {
                num = (num * -1) - 1;
                allocator.deallocate(numbers.at(num), sizeof(double));
                numbers.erase(numbers.begin() + num);
            }
        }
        allocator.print();
        if(x != numCases)
            std::cout << std::endl;
    }
    return 0;}
